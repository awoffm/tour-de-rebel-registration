package org.tdr.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.tdr.model.Team;

import java.util.List;

public interface TeamRepository extends CrudRepository<Team,Long> {

    Page<Team> findAll(Pageable pageRequest);

    Page<Team> findAllByNameContaining(String name, Pageable pageRequest);

    Team findById(long id);

    List<Team> findAllByLeaderId(long attendeesId);
}
