package org.tdr.repository;

import org.springframework.data.repository.CrudRepository;
import org.tdr.model.Country;

import java.util.List;

public interface CountryRespository extends CrudRepository<Country, Long> {

    Country findById(long id);

    Country findByName(String username);

    List<Country> findAll();
}
