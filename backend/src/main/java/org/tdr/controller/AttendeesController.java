package org.tdr.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.tdr.Converter;
import org.tdr.dto.AttendeesDTO;
import org.tdr.dto.RegistrationAttendessDTO;
import org.tdr.dto.TeamDTO;
import org.tdr.model.Attendees;
import org.tdr.security.*;
import org.tdr.service.AttendeesService;
import org.tdr.service.TeamService;

import java.util.List;

@RestController
@RequestMapping("/api/attendees")
public class AttendeesController {

    @Autowired
    private AttendeesService attendeesService;

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private JwtUserDetailsService userDetailsService;
    @Autowired
    private TeamService teamService;

    private RegistrationAttendessDTO fixAttendeesDTO(RegistrationAttendessDTO registrationAttendessDTO){
        registrationAttendessDTO.setUsername(StringUtils.trim(registrationAttendessDTO.getUsername()));
        registrationAttendessDTO.setMail(StringUtils.lowerCase(StringUtils.trim(registrationAttendessDTO.getMail())));
        if(registrationAttendessDTO.getFirstName() == null){
            registrationAttendessDTO.setFirstName("");
        }else{
            registrationAttendessDTO.setFirstName(StringUtils.trim(registrationAttendessDTO.getFirstName()));
        }
        if(registrationAttendessDTO.getSecondName() == null){
            registrationAttendessDTO.setSecondName("");
        }else{
            registrationAttendessDTO.setSecondName(StringUtils.trim(registrationAttendessDTO.getSecondName()));
        }
        if(registrationAttendessDTO.getIsCycling() == null){
            registrationAttendessDTO.setIsCycling(false);
        }
        if(registrationAttendessDTO.getIsOrga() == null){
            registrationAttendessDTO.setIsOrga(false);
        }
        if(registrationAttendessDTO.getIsPress() == null){
            registrationAttendessDTO.setIsPress(false);
        }
        if(registrationAttendessDTO.getPhone()==null){
            registrationAttendessDTO.setPhone("");
        }else{
            registrationAttendessDTO.setPhone(StringUtils.trim(registrationAttendessDTO.getPhone()));
        }
        if(registrationAttendessDTO.getPlz()==null){
            registrationAttendessDTO.setPlz("");
        }else{
            registrationAttendessDTO.setPlz(StringUtils.trim(registrationAttendessDTO.getPlz()));
        }
        return registrationAttendessDTO;
    }

    private AttendeesDTO fixAttendeesDTO(AttendeesDTO attendeesDTO){
        if(attendeesDTO.getIsCycling() == null){
            attendeesDTO.setIsCycling(false);
        }
        if(attendeesDTO.getIsOrga() == null){
            attendeesDTO.setIsOrga(false);
        }
        if(attendeesDTO.getIsPress() == null){
            attendeesDTO.setIsPress(false);
        }
        if(attendeesDTO.getPhone()==null){
            attendeesDTO.setPhone("");
        }else{
            attendeesDTO.setPhone(StringUtils.trim(attendeesDTO.getPhone()));
        }
        if(attendeesDTO.getPlz()==null){
            attendeesDTO.setPlz("");
        }else{
            attendeesDTO.setPlz(StringUtils.trim(attendeesDTO.getPlz()));
        }
        return attendeesDTO;
    }

    private void validateAttendees(RegistrationAttendessDTO registrationAttendessDTO){
        if(StringUtils.isEmpty(registrationAttendessDTO.getUsername()) || !StringUtils.isAlphanumeric(registrationAttendessDTO.getUsername()) || registrationAttendessDTO.getUsername().length() < 4 || registrationAttendessDTO.getUsername().length() > 20){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Username have to between 4 and 20 characters and alphanumeric");
        }
        if(StringUtils.isEmpty(registrationAttendessDTO.getMail()) || !registrationAttendessDTO.getMail().matches("^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$")){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Not a valid email format");
        }
        if(registrationAttendessDTO.getFirstName().length() > 40 || !StringUtils.isAlphanumericSpace(registrationAttendessDTO.getFirstName())){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"FirstName have to be alphanumeric and less then 40 characters");
        }
        if(registrationAttendessDTO.getSecondName().length() > 40 || !StringUtils.isAlphanumericSpace(registrationAttendessDTO.getSecondName())){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"SecondName have to be alphanumeric and less then 40 characters");
        }
        if((registrationAttendessDTO.getPhone().length()<6 && !registrationAttendessDTO.getPhone().isEmpty()) || registrationAttendessDTO.getPhone().length()>30){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Not a valid Phone number");
        }
        if(!registrationAttendessDTO.getPhone().isEmpty() && !StringUtils.isNumeric(registrationAttendessDTO.getPhone())){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Phone number have to numeric");
        }
        if(StringUtils.isEmpty(registrationAttendessDTO.getPassword()) || !registrationAttendessDTO.getPassword().matches("^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*#?&]{8,}$")){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Password dose not match conditions");
        }
        if(registrationAttendessDTO.getPlz().length() >= 30){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"not a valid plz");
        }
    }

    private void validateAttendees(AttendeesDTO attendeesDTO){
        if((attendeesDTO.getPhone().length()<6 && !attendeesDTO.getPhone().isEmpty()) || attendeesDTO.getPhone().length()>30){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Not a valid Phone number");
        }
        if(!attendeesDTO.getPhone().isEmpty() && !StringUtils.isNumeric(attendeesDTO.getPhone())){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Phone number have to numeric");
        }
        if(attendeesDTO.getPlz().length() >= 30){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"not a valid plz");
        }
    }

    @PostMapping
    ResponseEntity<AttendeesDTO> saveAttendess(@RequestBody AttendeesDTO attendeesDTO) {
        fixAttendeesDTO(attendeesDTO);
        validateAttendees(attendeesDTO);
        Attendees attendees = attendeesService.updateAttendees(attendeesDTO);
        return ResponseEntity.ok(Converter.convertAttendeesToAttendeesDTO(attendees));
    }

    @PostMapping("/create")
    ResponseEntity<JwtResponse> createAttendees(@RequestBody RegistrationAttendessDTO attendessDTO) {
        attendessDTO = fixAttendeesDTO(attendessDTO);
        validateAttendees(attendessDTO);
        Attendees attendees = attendeesService.createAttendees(attendessDTO);
        if (attendees == null) {
            return ResponseEntity.badRequest().build();
        }

        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(attendessDTO.getUsername(), attendessDTO.getPassword()));
        final UserDetails userDetails = userDetailsService
                .loadUserByUsername(attendessDTO.getUsername());
        final String token = jwtTokenUtil.generateToken(userDetails);
        return ResponseEntity.ok(new JwtResponse(token));
    }

    @GetMapping
    ResponseEntity<AttendeesDTO> getOwnAttendeesInfo() {
        MyUserPrincipal myUserPrincipal = (MyUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (myUserPrincipal == null) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }


        AttendeesDTO attendeesDTO = Converter.convertAttendeesToAttendeesDTO(myUserPrincipal.getAttendees());
        return ResponseEntity.ok(attendeesDTO);
    }

    @GetMapping("/teams")
    ResponseEntity<List<TeamDTO>> getTeams(){
        MyUserPrincipal myUserPrincipal = (MyUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return ResponseEntity.ok(Converter.convertTeamsToTeamDTOs(teamService.getTeamsByLeader(myUserPrincipal.getAttendees())));
    }
}
