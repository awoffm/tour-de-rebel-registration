package org.tdr;

import org.tdr.dto.*;
import org.tdr.model.Attendees;
import org.tdr.model.Country;
import org.tdr.model.Organisation;
import org.tdr.model.Team;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class Converter {

    public static AttendeesDTO convertAttendeesToAttendeesDTO(Attendees attendees) {
        return AttendeesDTO.builder()
                .id(attendees.getId())
                .username(attendees.getUsername())
                .mail(attendees.getMail())
                .firstName(attendees.getFirstName())
                .secondName(attendees.getSecondName())
                .phone(attendees.getPhone())
                .plz(attendees.getPlz())
                .isCycling(attendees.getIsCycling())
                .isOrga(attendees.getIsOrga())
                .isPress(attendees.getIsPress())
                .organisation(attendees.getOrganisation().getId())
                .country(attendees.getCountry().getId())
                .build();
    }

    public static OrganisationDTO convertOrganistationToOrganistationDTO(Organisation organisation) {
        return OrganisationDTO.builder().id(organisation.getId()).name(organisation.getName()).shortName(organisation.getShortName()).build();
    }

    public static List<OrganisationDTO> convertOrganistationsToOrganistationDTOs(Collection<Organisation> organisations) {
        return organisations.stream().map(Converter::convertOrganistationToOrganistationDTO).collect(Collectors.toList());
    }

    public static CountryDTO convertCountyToCountyDTO(Country country) {
        return CountryDTO.builder().id(country.getId()).name(country.getName()).shortName(country.getShortName()).build();
    }

    public static List<CountryDTO> convertCountiesToCountyDTOs(Collection<Country> countries) {
        return countries.stream().map(Converter::convertCountyToCountyDTO).collect(Collectors.toList());
    }

    public static TeamDTO convertTeamToTeamDTO(Team team){
            return TeamDTO.builder()
                    .id(team.getId())
                    .name(team.getName())
                    .description(team.getDescription())
                    .leader(convertAttendeesToTeamMemberDTO(team.getLeader()))
                    .members(convertAttendeesetoTeamMemberDTOs(team.getMembers()))
                    .build();
    }

    public static List<TeamDTO> convertTeamsToTeamDTOs(Collection<Team> countries) {
        return countries.stream().map(Converter::convertTeamToTeamDTO).collect(Collectors.toList());
    }

    public static List<TeamMemberDTO> convertAttendeesetoTeamMemberDTOs(Collection<Attendees> countries) {
        return countries.stream().map(Converter::convertAttendeesToTeamMemberDTO).collect(Collectors.toList());
    }

    public static TeamMemberDTO convertAttendeesToTeamMemberDTO(Attendees user){
        return TeamMemberDTO.builder()
                .username(user.getUsername())
                .id(user.getId())
                .firstName(user.getFirstName())
                .build();
    }
}

