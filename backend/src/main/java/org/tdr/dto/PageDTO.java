package org.tdr.dto;

import lombok.*;

import java.util.List;

@Getter
@AllArgsConstructor
@Builder
@NoArgsConstructor
@Setter
public class PageDTO<T> {
    int page;
    int totalPages;
    long totalElements;

    List<T> elements;
}
