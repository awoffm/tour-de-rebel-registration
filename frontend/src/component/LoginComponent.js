import React, {useContext, useState} from 'react'
import {useHistory} from "react-router-dom";
import RestService from "../service/RestService"
import UserContext from "../service/UserContext";

function LoginComponent(props) {

    let history = useHistory();
    const userContext = useContext(UserContext);

    const [credentials, setCredentials] = useState({
        username: "test",
        password: "password"
    });

    const edit = (param, value) => {
        let att = {...credentials};
        att[param] = value;
        setCredentials(att);
    };

    const RegisterButton = () => (
        <button
            type='button'
            onClick={() => {
                RestService.login(credentials).then(res => {
                    userContext.setToken(res);
                    history.push('/')
                });
            }}
        >
            Login
        </button>
    );


    return (<div className="Login">
            <h2>Benutzer Anmelden</h2>
            <p>
                Username*:
                <input value={credentials.username} onChange={(ev) => edit("username", ev.target.value)}/>
            </p>
            <p>
                Password*:
                <input type="password" value={credentials.password}
                       onChange={(ev) => edit("password", ev.target.value)}/>
            </p>
            <RegisterButton/>
        </div>


    );
}

export default LoginComponent;

