import React from 'react'
import CreateTeamComponent from "./CreateTeamComponent";
import TeamPageComponent from "./TeamPageComponent";

function TeamsComponent() {
    return(<div>
        <h2>Teams</h2>
        <h3>Teams suchen</h3>
        <TeamPageComponent/>
        <h3>Team erstellen</h3>
        <CreateTeamComponent/>
    </div>)
}

export default TeamsComponent;